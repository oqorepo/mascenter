const gulp = require('gulp'),
    browserSync = require('browser-sync').create(), //Recarga el navegador
    autoprefixer = require('gulp-autoprefixer'), //Autoprefixer
    cleanCSS = require('gulp-clean-css'), //Minifica CSS
    imagemin = require('gulp-imagemin'), //Minifica Imagenes
    notify = require('gulp-notify'), //Notificaciones
    pug = require('gulp-pug'), //Copila PUG
    rename = require('gulp-rename'), //Renombra archivos
    sass = require('gulp-sass'), //Copila SCSS
    sourcemaps = require('gulp-sourcemaps'), //SourceMaps
    uglify = require('gulp-uglify'); //Minifica JS

gulp.task('sass', function () {
    return gulp.src('./src/scss/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'expanded',
            sourceComments: true
        }))
        .on("error", notify.onError({
            sound: true,
            title: 'Error de copilación SCSS'
        }))
        .pipe(autoprefixer({
            versions: ['last 2 browsers']
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./dist/assets/css/'));
});

gulp.task('minify-css', function () {
    return gulp.src('./dist/assets/css/main.css')
        //.pipe(rename({suffix: '-dist'}))
        .pipe(sourcemaps.init({
            loadMaps: true
        }))
        //.pipe(cleanCSS())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./dist/assets/css/'))
        .pipe(notify({
            sound: false,
            message: 'Estilos Procesados',
            onLast: true
        }))
        .pipe(browserSync.stream({
            match: '**/*.css'
        }));
});

gulp.task('pug', function () {
    return gulp.src('./src/pug/pages/**/*.pug')
        .pipe(pug({
            pretty: true
        }))
        .on('error', notify.onError(function (error) {
            return 'Error de copilacion PUG.\nDetalles en la consola.\n' + error;
        }))
        .pipe(gulp.dest('./dist/'))
        .pipe(notify({
            sound: false,
            message: 'PUG Procesados',
            onLast: true
        }))
        .pipe(browserSync.stream());
});

gulp.task('minify-js', function () {
    return gulp.src('./src/js/main.js')
        .pipe(uglify()
            .on("error", notify.onError({
                sound: true,
                title: 'Error en JS'
            })))
        .pipe(rename({
            suffix: '-dist'
        }))
        .pipe(gulp.dest('./dist/assets/js/'))
        .pipe(notify({
            sound: false,
            message: 'Js Procesados',
            onLast: true
        }));
});

gulp.task('images', function () {
    gulp.src('./src/images/*')
        .pipe(imagemin({
            optimizationLevel: 5,
            progressive: true,
            interlaced: true
        }))
        .pipe(gulp.dest('./dist/img'));
});

gulp.task('watch', function () {
    browserSync.init({
        server: './dist',
        files: ['./*.pug', './*.css', './*.js', './*.scss', './*.jpg', './*.png'],
        notify: true
    });
    gulp.watch('./src/scss/**/*.scss', ['sass']);
    gulp.watch('./dist/**/*.css', ['minify-css']);
    gulp.watch('./src/pug/**/*.pug', ['pug']);
    gulp.watch('./dist/**/*.html').on('change', browserSync.reload);
    gulp.watch('./src/**/*.js', ['minify-js']);
});

gulp.task('default', ['sass', 'minify-css', 'pug', 'minify-js', 'images']);